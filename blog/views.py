from django.shortcuts import render, get_object_or_404
from .models import Post, Comment
from django.views.generic import ListView
from .forms import EmailPostForm, CommentForm
from django.core.mail import send_mail

def post_list(request):
	posts = Post.published.all()
	paginator = Paginator(object_list, 3) # 3 posts per page
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		#if page is not an integer deliver the first page
		posts =paginator.page(1)
	except EmptyPage:
		#if page is out of range deliver the last page results
		posts = paginator.page(paginator.num_pages)
	return  render(request, 'blog/post/list.html',{'page': page,'posts': posts})

def post_detail(request, year, month, day, post):
	post = get_object_or_404(Post, slug=post,\
		status='published',publish__year=year,\
		publish__month=month,publish__day=day)

	comments = post.comments.filter(active=True) # this queryset lists active comments for this blogpost/article
	if request.method=='POST':
		#A comment was posted
		comment_form = CommentForm(data=request.POST)
		if comment_form.is_valid():
			#create Comment Object but do not save to database
			new_comment = comment_form.save(commit=False)

			#assign the current post to the comment
			new_comment.post = post

			#Save the comment to the database
			new_comment.save()
	else:
		comment_form = CommentForm()

	return render(request,'blog/post/detail.html',{'post', post, 'comments', comments,'comment_form', comment_form})

class PostListView(ListView):
	"""docstring for PostListView"""
	queryset = Post.published.all()
	context_object_name = 'posts'
	paginate_by = 3
	template_name = 'blog/post/list.html'

#we want to share posts via email so

def post_share(request, post_id): 
#retrieve post by ID
	post = get_object_or_404(Post, id=post_id, status='published')# making sure we retrive posts by id and published status
	sent = False

	if request.method == 'POST':
		#form was submitted
		form = EmailPostForm(request.POST) #this is POST to send data not the articles
		if  form.is_valid():
			cd = form.cleaned_data  #Form passed validation, returns an empty form fields and stores their values
			post_url = request.build_absolute_url(post.get_absolute_url())
			subject = '{} ({}) recommends you reading "{}"'.format(cd['name'], cd['email'], post.title)
			message = 'Read "{}" at {}\n\n{}\'s comments: {}'. format(post.title, post_url, cd['name'], cd['comments'])
			send_mail(subject, message, 'lionelnyawose@gmail.com', [cd['to']])
			sent = True

	else:
		form = EmailPostForm()  # defined in forms.py

		return render(request, 'blog/post/share.html',{'post', post, 'form', form, 'sent', sent})

	


		



	

# Create your views here.
